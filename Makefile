.SILENT :
.PHONY : lgtm clean fmt

TAG:=`git describe --abbrev=0 --tags`
LDFLAGS:=-X main.buildVersion=$(TAG)

all: lgtm

lgtm:
	echo "Building lgtm $(LDFLAGS)"
	go install -ldflags "$(LDFLAGS)"

build:
	GOOS=linux GOARCH=amd64 go build -o lgtm -mod=vendor -ldflags="-s -w"

build_alpine:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o lgtm -mod=vendor -ldflags="-s -w"