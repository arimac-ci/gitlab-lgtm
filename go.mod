module gitlab.com/arimac-ci/gitlab-lgtm

go 1.16

require (
	github.com/Sirupsen/logrus v1.0.0
	github.com/boltdb/bolt v1.3.0
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20210910150752-751e447fb3d0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
